<?php
/**
 * @file
 * Sample template for gigulate news item.
 * You can use any variables within a gigulate news story:
 * @see return value at http://gigulate.com/api/docs/news.stories
 * Example:
 * This template is passed the story tag as $item - so you can use $item->title to access the title tag below:
 * ...<story id="[Article ID]">
 *      <title>[Article Title]</title>
 * 
 * As of Gigulate API v.1.0:
 * $item['id']
 * $item->title
 * $item->url
 * $item->description
 * $item->{'date.published'}
 * $item->artists['total']
 * $item->artists->artist (array) 
 * $item->artists->artist[0]['id']
 * $item->artists->artist[0]['mbid'] (MusicBrainz ID)
 * $item->artists->artist[0]->name
 * $item->artists->artist[0]->url
 * $item->artists->artist[0]->images[{'attribution.name'}]
 * $item->artists->artist[0]->images[{'attribution.url'}]
 * $item->artists->artist[0]->images->image (array)
 * $item->artists->artist[0]->images->image[0]['src']
 * $item->artists->artist[0]->images->image[0]['size'] (additional|feature|mini)
 * $item->source['id']
 * $item->source->name
 * $item->source->url
 * 
 **/
?>
<div class="gigulate">
  <h1><?php echo check_markup($item->title); ?></h1>
  <?php 
  if($item->artists->artist[0]->images->image[1]['src']) {
  	echo theme('image', $item->artists->artist[0]->images->image[1]['src'], $item->title, $item->title, NULL, FALSE); 
  }
  ?>
  <div class="description"><?php echo check_markup($item->description); ?></div>
  <div class="link"><?php echo l('Read more', $item->url); ?></div>
  <div class="source"><?php echo l($item->source->name, $item->source->url); ?></div>
</div>