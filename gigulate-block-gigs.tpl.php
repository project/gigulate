<?php
/**
 * @file
 * Sample template for gigulate news block.
 * You can use any variables within a gigulate news story resultset:
 * @see return value at http://gigulate.com/api/docs/gig.gigs
 * Example:
 * This template is passed the gigs.gigs tag as $items 
 **/
?>
<?php
if($items) {
	$list_items = array();
	foreach($items as $item){
	  $list_items[] = theme('gigulate_block_gigs_item', $item);
	}
	echo theme('item_list', $list_items);
}  
?>