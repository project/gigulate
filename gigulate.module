<?php
/**
 * @file
 * Gigulate module - integration with the Gigulate API @see http://gigulate.com/api for info
 * Produced by Adub - http://adub.com
 */
 
/**
 * Implementation of hook_help()
 * @param path which path of the site we're displaying help
 * @param arg array that holds the current path as would be returned from arg() function
 * @return help text for the path
 */
function gigulate_help($path, $arg) {
  $output = '';  
  switch ($path) {
    case "admin/help#gigulate":
      $output = '<p>'.  t("Gigulate API v.1.0. Requires an API key. See http://gigulate.com/api for more info.") .'</p>';
      break;
  }
  return $output;
} 

/**
 * Implementation of hook_perm()
 * @return array An array of valid permissions for the gigulate module
 */
function gigulate_perm() {
  return array('gigulate admin');
} 

/**
 * Implementation of hook_admin()
 * @see http://gigulate.com/api for API keys
 * @return array Form
 */
function gigulate_admin() {
  $form = array();

  $form['gigulate_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('gigulate_key', ''),
    '#size' => 20,
    '#maxlength' => 20,
    '#description' => t('Visit '. l('Gigulate', 'http://gigulate.com/api') .' to get a key.'),
    '#required' => TRUE,
  );

  $form['gigulate_api_version'] = array(
    '#type' => 'select',
    '#title' => t('API version'),
    '#default_value' => variable_get('gigulate_api_version', '1.0'),
    '#options' => array(
      '1.0' => '1.0',
    ),
    '#required' => TRUE,
  );

  if (module_exists('geoip') && variable_get('geoip_data_file', FALSE) == drupal_get_path('module', 'geoip') .'/data/GeoLiteCity.dat') {
     $form['gigulate_geoip'] = array(
      '#type' => 'fieldset',
      '#title' => t('GeoIP module settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['gigulate_geoip']['gigulate_geoip_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use GeoIP lookup by city'),
      '#default_value' => variable_get('gigulate_geoip_enabled', NULL),
    );
    $form['gigulate_geoip']['gigulate_geoip_radius'] = array(
      '#type' => 'textfield',
      '#title' => t('GeoIP radius in km'),
      '#default_value' => variable_get('gigulate_geoip_radius', '50'),
      '#size' => 3,
      '#maxlength' => 3,
      '#required' => TRUE,
    );    
  }

  return system_settings_form($form);
} 

/**
 * Set cache time in minutes
 * @return array Form
 */
function gigulate_admin_cache() {
  $form = array();

  $form['gigulate_cache_time'] = array(
    '#type' => 'select',
    '#title' => t('Local cache time'),
    '#default_value' => variable_get('gigulate_cache_time', 'cron'),
    '#options' => array(
      'cron' => t('Refresh on cron'),
      '60' => t('1 hour'),
      '720' => t('12 hours'),
      '2880' => t('2 days'),
    ),
    '#description' => t("Minimum cache lifetime (content will refresh on the next cron run after this time has elapsed)"),
    '#required' => TRUE,
  );

  return system_settings_form($form);
} 

/**
 * Set vocabularies to be used for query and artist lookups
 * @return array Form
 */
function gigulate_admin_taxonomy() {
  $form = array();

  $vocabularies = array('' => 'None'); // optional
  foreach (taxonomy_get_vocabularies() as $vocab) {
    $vocabularies[$vocab->vid] = $vocab->name; 
  }

  $form['gigulate_taxonomy_news'] = array(
    '#type' => 'fieldset',
    '#title' => t('News settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['gigulate_taxonomy_news']['gigulate_news_query_vocab'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary for searching related news stories'),
    '#default_value' => variable_get('gigulate_news_query_vocab', NULL),
    '#options' => $vocabularies,
    '#description' => t("The first term found in this vocabulary will be used."),
    '#required' => FALSE,
  );

  $form['gigulate_taxonomy_news']['gigulate_news_artist_vocab'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary for matching artists to related news stories'),
    '#default_value' => variable_get('gigulate_news_artist_vocab', NULL),
    '#options' => $vocabularies,
    '#description' => t("All terms found in this vocabulary will be used."),
    '#required' => FALSE,
  );

  $form['gigulate_taxonomy_gigs'] = array(
    '#type' => 'fieldset',
    '#title' => t('Gigs settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['gigulate_taxonomy_gigs']['gigulate_gigs_artist_vocab'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary for matching artists to related gigs'),
    '#default_value' => variable_get('gigulate_gigs_artist_vocab', NULL),
    '#options' => $vocabularies,
    '#description' => t("All terms found in this vocabulary will be used."),
    '#required' => FALSE,
  );
  
  return system_settings_form($form);
  
} 


/**
 * Implementation of hook_admin_validate()
 */
function gigulate_admin_validate($form, &$form_state) {
    
  $gigulate_key = $form_state['values']['gigulate_key'];

  $result = drupal_http_request(_gigulate_testurl($gigulate_key));

  if ($result->code != '200') {
    form_set_error('gigulate_key', t('There seems to be a network error: '. $result->code));
    return;
  }

  $content = new SimpleXMLElement($result->data);
  if ($content->error) {
    form_set_error('gigulate_key', t("Gigulate reports the following error: ". $content->error));
  }

} 

/**
 * Implementation of hook_menu()
 */
function gigulate_menu() {

  $items = array();

  $items['admin/settings/gigulate'] = array(
    'title' => 'Gigulate',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gigulate_admin'),
    'access arguments' => array('gigulate admin'),
    'type' => MENU_NORMAL_ITEM,
   );

  $items['admin/settings/gigulate/api'] = array(
    'title' => 'API',
    'description' => 'Gigulate API settings',
    'access arguments' => array('gigulate admin'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
   );

  $items['admin/settings/gigulate/taxonomy'] = array(
    'title' => 'Taxonomy',
    'description' => 'Gigulate taxonomy settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gigulate_admin_taxonomy'),
    'access arguments' => array('gigulate admin'),
    'type' => MENU_LOCAL_TASK,
   );
   
   $items['admin/settings/gigulate/cache'] = array(
     'title' => 'Performance',
     'description' => 'Gigulate cache settings',
     'page callback' => 'drupal_get_form',
     'page arguments' => array('gigulate_admin_cache'),
     'access arguments' => array('gigulate admin'),
     'type' => MENU_LOCAL_TASK,
    );
    
    $items['gigulate/news'] = array(
      'title' => 'Gigulate',
      'page callback' => 'gigulate_news_story',
      'access arguments' => array('access content'), 
      'type' => MENU_CALLBACK
    );

  return $items;
} 


/**
 * Implementation of hook_block()
 * @param string $op one of "list", "view", "save" and "configure"
 * @param integer $delta code to identify the block
 * @param array $edit only for "save" operation
 **/
function gigulate_block($op = 'list', $delta = 0, $edit = array()) { 

 if ($op == "list") {
    // Generate listing of blocks from this module, for the admin/block page
    $block = array();
    $block[0]["info"] = t('Gigulate News');
    $block[1]["info"] = t('Gigulate Gigs By This Artist');
    $block[2]["info"] = t('Gigulate Upcoming Gigs');
    return $block;
  }
  else if ($op == 'view') {

    switch ($delta) {
      case 0:
        $content = _gigulate_fetch(_gigulate_url()); 
        $block['subject'] = 'Gigulate News';
        $block['content'] = theme(array("gigulate_{$delta}_block", 'gigulate_block_news'), $content->{'news.stories'}->story);
        return $block;
      case 1:
        $content = _gigulate_fetch(_gigulate_url('gigs.gigs'));  
        $block['subject'] = 'Gigulate Gigs By This Artist';
        $block['content'] = theme(array("gigulate_{$delta}_block", 'gigulate_block_gigs_artist'), $content->{'gigs.gigs'}->gig);
        return $block;
      case 2:
        $content = _gigulate_fetch(_gigulate_url('gigs.gigs', FALSE)); // don't filter by artist
        $block['subject'] = 'Gigulate Upcoming Gigs';
        $block['content'] = theme(array("gigulate_{$delta}_block", 'gigulate_block_gigs'), $content->{'gigs.gigs'}->gig);
        return $block;
      }
  }
} 

/**
 * Gets a single story for page view
 * @param integer $id Gigulate news story ID 
 */
function gigulate_news_story($id) {
    $url = _gigulate_baseurl('news.stories');
    $url .= '&id='. $id;
    $content = _gigulate_fetch($url);
    return theme(array('gigulate_page_news'), $content->{'news.stories'}->story);
}
    
/**
 * Implementation of hook_theme()
 */
function gigulate_theme() { 
  $theme = array(
    'gigulate_block_news' => array(
      'arguments' => array('items' => NULL),
      'template' => 'gigulate-block-news',
    ),
    'gigulate_block_gigs' => array(
      'arguments' => array('items' => NULL),
      'template' => 'gigulate-block-gigs',
    ),
    'gigulate_block_gigs_artist' => array(
      'arguments' => array('items' => NULL),
      'template' => 'gigulate-block-gigs-artist',
    ),
    'gigulate_block_news_item' => array(
      'arguments' => array('item' => NULL),
      'template' => 'gigulate-block-news-item',
    ),
    'gigulate_block_gigs_item' => array(
      'arguments' => array('item' => NULL),
      'template' => 'gigulate-block-gigs-item',
    ),
    'gigulate_page_news' => array(
      'arguments' => array('item' => NULL),
      'template' => 'gigulate-page-news',
    ),
  );
  return $theme;
} 

/**
 * Implementation of hook_flush_caches()
 */
function gigulate_flush_caches() {
  return array('cache_gigulate');
}

/** 
 * Fetches and caches resultset
 * @param string $url URL for query 
 * @return SimpleXMLElement Gigulate resultset
 */
function _gigulate_fetch($url) {
    $cache_table = 'cache_gigulate';
    $cacheid = md5($url);

    if (!$xml = cache_get($cacheid, $cache_table)) {

      $cache_time = variable_get('gigulate_cache_time', '5');
      if ($cache_time == 'cron') {
        $cache_until = CACHE_TEMPORARY;
      }
      else {
        $cache_until = time() + ((int)$cache_time * 60);
      }

      $result = drupal_http_request($url);

      if ($result->code != '200') {
        $message = t('There seems to be a network error: '. $result->code);
        watchdog('gigulate', $message, array(), WATCHDOG_ERROR);
        drupal_set_message($message);
        return;
      }

      $xml = $result->data;
      if ($xml->error) {
        $message = t('Gigulate reports the following error: '. $xml->error);
        watchdog('gigulate', $message, array(), WATCHDOG_ERROR);
        drupal_set_message($message);
        return;
      }
      cache_set($cacheid, $xml, $cache_table, $cache_until);
    }
    else {
      $xml = $xml->data; 
    }
    try {
      $content = new SimpleXMLElement($xml);
      return $content;
    } catch (Exception $e) {
      watchdog('gigulate', $e->getMessage() .' - '. $url, array(), WATCHDOG_ERROR);
    }

}

/**
 * Builds basic URL for testing key/connection
 * @param string $key Gigulate API key 
 * @param string $endpoint Gigulate endpoint 
 * @return string URL
 */
function _gigulate_testurl($key, $endpoint = 'news.stories') {
  
  $apiversion = variable_get('gigulate_api_version', '1.0');
  $url = "http://api.gigulate.com/{$apiversion}/{$endpoint}";
  $url .= "?key={$key}&limit=2";
  return $url;
}

/**
 * Builds basic URL that will connect
 * @param string $endpoint Gigulate endpoint 
 * @return string URL
 */
function _gigulate_baseurl($endpoint = 'news.stories') {
  if (!$gigulate_key = variable_get('gigulate_key', FALSE)) {
    return;
  }
  
  $apiversion = variable_get('gigulate_api_version', '1.0');
  $url = "http://api.gigulate.com/{$apiversion}/{$endpoint}";
  $url .= "?key={$gigulate_key}";
  return $url;
}

/**
 * Builds URL with complete query
 * @param string $endpoint Gigulate endpoint 
 * @param bool $filter_by_artist Whether to filter by artist 
 * @return string URL
 */
function _gigulate_url($endpoint = 'news.stories', $filter_by_artist = TRUE) {
    $limit = 5;
    $url = _gigulate_baseurl($endpoint) ."&limit={$limit}&order=date.desc";

    if ($endpoint == 'news.stories') {

      $query_vocab = variable_get('gigulate_news_query_vocab', FALSE);
      if ($query_vocab && $query = _gigulate_get_firstterm($query_vocab)) {
        $url .= '&q='. $query;
      }
      
      $query_vocab = variable_get('gigulate_news_artist_vocab', FALSE); 
      if ($query_vocab && $artist = _gigulate_get_terms($query_vocab)) {
        // TODO add musicbrainz option
        $url .= '&artists='. $artist;
      }
    } 
    elseif ($endpoint == 'gigs.gigs' && $filter_by_artist) {
      
      $query_vocab = variable_get('gigulate_gigs_artist_vocab', FALSE);
      if ($query_vocab && $artist = _gigulate_get_terms($query_vocab)) {
        // TODO add musicbrainz option
        $url .= '&artists='. $artist;
      }
    }

    if ($endpoint == 'gigs.gigs' && variable_get('gigulate_geoip_enabled', FALSE) && module_exists('geoip')) { 
      $geoip = geoip_city();  
      $url .= '&locale=['. $geoip->longitude .'],['. $geoip->latitude .'],['. variable_get('gigulate_geoip_radius', NULL) .']';
    }

    return $url;
}

/**
 * Helper
 * @param int Vocabulary id
 * @return First term found in the vocab for the current node
 */
function _gigulate_get_firstterm($vid) {
  $node = node_load(arg(1));
  if ($node) {
    foreach ($node->taxonomy as $term) {
      if ($term->vid == $vid) {
          return drupal_urlencode($term->name); 
      }
    }
  }
  return FALSE;
}

/**
 * Helper
 * @param int Vocabulary id
 * @return Comma-delimited string of all terms found in the vocab for the current node
 */
function _gigulate_get_terms($vid) {
  $node = node_load(arg(1));
  if ($node) {
    $terms = array();
    foreach ($node->taxonomy as $term) {
      if ($term->vid == $vid) {
          $terms[] = drupal_urlencode($term->name); 
      }
    }
    return implode(',', $terms);
  }
  return FALSE;
}


