<?php
/**
 * @file
 * Sample template for gigulate news block.
 * You can use any variables within a gigulate news story resultset:
 * @see return value at http://gigulate.com/api/docs/news.stories
 * Example:
 * This template is passed the news.stories tag as $items 
 **/
?>
<?php
if($items) {
	$list_items = array();
	foreach($items as $item){
	  $list_items[] = theme('gigulate_block_news_item', $item);
	}
	echo theme('item_list', $list_items);
}  
?>