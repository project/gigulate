<?php
/**
 * @file
 * Sample template for gigulate gigs item.
 * You can use any variables within a gigulate gig:
 * @see return value at http://gigulate.com/api/docs/gigs.gigs
 * Example:
 * This template is passed the gig tag as $item - so you can use $item->date to access the date tag below:
 * ...<gig id="[Gig ID]">
 *      <date>[Gig Date ISO8601]</date>
 * 
 * As of Gigulate API v.1.0:
 * $item['id']
 * $item->date
 * $item->url
 * $item->artists['total']
 * $item->artists->artist (array) 
 * $item->artists->artist[0]['id']
 * $item->artists->artist[0]['mbid'] (MusicBrainz ID)
 * $item->artists->artist[0]->name
 * $item->artists->artist[0]->url
 * $item->artists->artist[0]->images[{'attribution.name'}]
 * $item->artists->artist[0]->images[{'attribution.url'}]
 * $item->artists->artist[0]->images->image (array)
 * $item->artists->artist[0]->images->image[0]['src']
 * $item->artists->artist[0]->images->image[0]['size'] (additional|feature|mini)
 * $item->venue['id']
 * $item->venue->name
 * $item->venue->url
 * $item->venue->address
 * $item->venue->locale (empty tag)
 * $item->venue->locale['lon']
 * $item->venue->locale['lat']
 * 
 **/
?>
<h6><?php echo l($item->artists->artist[0]->name, $item->url, array('html'=>true)); ?></h6>
<?php 
echo $item->venue->name.' - '.$item->date;
?>


